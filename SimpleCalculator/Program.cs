﻿using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace SimpleCalculator
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, double> FuncArgs = new Dictionary<string, double>();
            FuncArgs.Add("pi", Math.PI);
            
            Calculator calculator = new Calculator(FuncArgs);

            while (true)
            {
                var command = Console.ReadLine();
                switch (command)
                {
                    case "calc":
                        Console.WriteLine("Type expression:");
                        Stack<string> stack = calculator.GetStack(Calculator.GetList(Console.ReadLine()));
                        
                        foreach (var st in stack)
                        {
                            Console.Write($" {st}");
                        }

                        calculator.CalcStack(stack);
                        Console.WriteLine($"\nanswer {stack.Pop()}");
                        break;
                    
                    case "AddFunc":
                        Console.WriteLine("Example: Func; 4; sin(arg3)/cos(arg2)-cos(arg1)/sin(arg0)\nType expression:");
                        var func = Console.ReadLine().Replace(" ", "").Split(";");
                        int argNum;
                        
                        try
                        {
                            argNum = int.Parse(func[1]);
                        }
                        catch (FormatException ex)
                        {
                            Console.WriteLine($"Can't parse {func[1]} to int. exception msg: {ex.Message}");
                            break;
                        }
                        
                        calculator.AddFunc(func[0], argNum, func[2]);
                        if (calculator.userFunctions[func[0]] == null) Console.WriteLine("fail");
                        break;
                    
                    case "AddParam":
                        Console.WriteLine("Example: a;15\nType param:");
                        var param = Console.ReadLine().Replace(" ", "").Split(";");
                        double value;
                        try
                        {
                            value = double.Parse(param[1]);
                            calculator.Args.Add(param[0], value);
                        }
                        catch (FormatException ex)
                        {
                            Console.WriteLine($"Can't parse {param[1]} to int. exception msg: {ex.Message}");
                        }
                        catch (ArgumentException ex)
                        {
                            Console.WriteLine($"Param with name {param[0]} already exist");
                        }
                        
                        break;
                    case "exit":
                        return;
                }
            }
        }
    }
}
