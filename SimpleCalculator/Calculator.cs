﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq.Expressions;
using System.Text;
using System.Text.RegularExpressions;

namespace SimpleCalculator
{
    class Calculator
    {
        private const string Operators = "+-*/^";
        private const string BadAmountOfBrackets = "Different amount of brackets";
        private const string BadName = "Unknown name";
        private const string BadExpression = "Wrong expression";
        private const string BadArgsCount = "Wrong amount of args in function";

        private static Dictionary<string, Function> funcs = new Dictionary<string, Function>
        {
            {
                "sin", new Function((x) =>
                {
                    if (x.Length != 1) throw new ArgumentException(BadArgsCount + " sin");
                    return Math.Sin(x[0]);
                }, 1)
            },
            {
                "cos", new Function((x) =>
                {
                    if (x.Length != 1) throw new ArgumentException(BadArgsCount + " cos");
                    return Math.Cos(x[0]);
                }, 1)
            },
            {
                "tan", new Function((x) =>
                {
                    if (x.Length != 1) throw new ArgumentException(BadArgsCount + " tan");
                    return Math.Tan(x[0]);
                }, 1)
            },
            {
                "ln", new Function((x) =>
                {
                    if (x.Length != 1) throw new ArgumentException(BadArgsCount + " ln");
                    return Math.Log(x[0]);
                }, 1)
            },
            {
                "pow", new Function((x) =>
                {
                    if (x.Length != 2) throw new ArgumentException(BadArgsCount + " pow");
                    return Math.Pow(x[0], x[1]);
                }, 2)
            }
        };

        public Dictionary<string, Function> userFunctions;
        public Dictionary<string, double> Args { get; set; }

        public Calculator(Dictionary<string, double> args)
        {
            Args = args;
            userFunctions = new Dictionary<string, Function>();
        }

        public Calculator(Dictionary<string, Function> userFunctions)
        {
            this.userFunctions = userFunctions;
            Args = new Dictionary<string, double>();
        }

        public Calculator(Dictionary<string, double> args, Dictionary<string, Function> userFunctions)
        {
            this.userFunctions = userFunctions;
            Args = args;
        }

        public void AddFunc(string Name, Function func)
        {
            if (funcs.ContainsKey(Name) || userFunctions.ContainsKey(Name))
            {
                throw new Exception($"Function with name {Name} already exist");
            }

            userFunctions.Add(Name, func);
        }

//todo подумать
        public void AddFunc(string Name, int argc, string expression)
        {
            Stack<string> stack = GetStack(GetList(expression));
            Regex regex = new Regex(@"^arg\d$"); // arg1 arg2 arg3 ...
            userFunctions.Add(Name, new Function((x) =>
            {
                var list = new List<string>(stack);
                for (int i = 0; i < list.Count; i++)
                {
                    if (regex.IsMatch(list[i]))
                    {
                        int argNum = int.Parse(list[i].Substring(3, list[i].Length - 3)); // num from arg1
                        if (argNum >= argc)
                            throw new Exception($"Number of arg({list[i]}) more than amount of args");
                        list[i] = x[argNum].ToString();
                    }
                }

                list.Reverse();
                Stack<string> calcStack = new Stack<string>(list);
                CalcStack(calcStack);

                return double.Parse(calcStack.Pop());
            }, argc));
        }

        public void RemoveFunc(string Name)
        {
            if (!funcs.ContainsKey(Name))
            {
                throw new Exception($"Function with name {Name} not found");
            }

            funcs.Remove(Name);
        }

        private static bool IsOperator(string element)
        {
            return Operators.IndexOf(element) != -1;
        }

        private static string FormatExpression(string expression)
        {
            expression = expression.Replace(" ", "");

            //todo rewrite this shit
            expression = expression.Replace("+", " + ");
            expression = expression.Replace("-", " - ");
            expression = expression.Replace("*", " * ");
            expression = expression.Replace("/", " / ");
            expression = expression.Replace("^", " ^ ");
            expression = expression.Replace("(", " ( ");
            expression = expression.Replace(")", " ) ");
            expression = expression.Replace(",", " , ");
            expression = expression.Replace("  ", " "); //two brackets makes to spaces (( -> (  (.
            expression = expression.Trim();
            return expression;
        }

        public static List<string> GetList(string expression)
        {
            expression = FormatExpression(expression);
            List<string> func = new List<string>(expression.Split(" "));
            func.Add("end");
            return func;
        }

        public Stack<string> GetStack(List<string> func)
        {
            Stack<string> calcStack = new Stack<string>();
            Stack<string> buffStack = new Stack<string>();
            buffStack.Push("start");

            int i = 0;
            while (i < func.Count)
            {
                //brackets 
                if (func[i] == "(")
                {
                    buffStack.Push(func[i]);
                    ++i;
                    continue;
                }

                if (func[i] == ")")
                {
                    if (buffStack.Peek() == "start") throw new Exception(BadAmountOfBrackets);
                    if (buffStack.Peek() == "(")
                    {
                        buffStack.Pop();
                        ++i;
                    }
                    else
                    {
                        calcStack.Push(buffStack.Pop());
                    }

                    continue;
                }

                // +-
                if (func[i] == "+" || func[i] == "-")
                {
                    if (buffStack.Peek() == "start" || buffStack.Peek() == "(")
                    {
                        buffStack.Push(func[i]);
                        ++i;
                    }
                    else
                    {
                        calcStack.Push(buffStack.Pop());
                    }

                    continue;
                }

                // */
                if (func[i] == "*" || func[i] == "/")
                {
                    if (buffStack.Peek() == "start" || buffStack.Peek() == "(" || buffStack.Peek() == "+" ||
                        buffStack.Peek() == "-")
                    {
                        buffStack.Push(func[i]);
                        ++i;
                    }
                    else
                    {
                        calcStack.Push(buffStack.Pop());
                    }

                    continue;
                }

                // ^
                if (func[i] == "^")
                {
                    //todo rewrite condition
                    if (buffStack.Peek() == "start" || buffStack.Peek() == "(" || buffStack.Peek() == "+" ||
                        buffStack.Peek() == "-" || buffStack.Peek() == "*" || buffStack.Peek() == "/")
                    {
                        buffStack.Push(func[i]);
                        ++i;
                    }
                    else
                    {
                        calcStack.Push(buffStack.Pop());
                    }

                    continue;
                }

                Function function = null;
                if (funcs.ContainsKey(func[i])) function = funcs[func[i]];
                if (userFunctions.ContainsKey(func[i])) function = userFunctions[func[i]];

                
                
                if (function != null)
                {
                    buffStack.Push(func[i]);

                    if (i >= func.Count - 1 && func[i + 1] != "(")
                    {
                        throw new Exception("Missing ( after function name.");
                    }

                    i += 2; //skip bracket 
                    int bracketsCounter = 0;
                    List<string> subFunc = new List<string>();
                    Stack<string> revStack = new Stack<string>();

                    for (int j = 0; j < function.ArgsCount; ++j)
                    {
                        subFunc.Clear();

                        //reading argument
                        while (i < func.Count)
                        {
                            if (func[i] == "(") ++bracketsCounter;
                            if (func[i] == ")")
                            {
                                --bracketsCounter;
                                if (bracketsCounter < 0)
                                {
                                    if (j == function.ArgsCount - 1)
                                    {
                                        i++; //skip ,
                                        subFunc.Add("end");
                                        break;
                                    }

                                    throw new ArgumentException(BadArgsCount + $" {buffStack.Peek()}");
                                }
                            }

                            if (func[i] == "," && bracketsCounter == 0)
                            {
                                i++; //skip ,
                                subFunc.Add("end");
                                break;
                            }

                            subFunc.Add(func[i++]);
                        }

                        if (i == func.Count && bracketsCounter >= 0) throw new ArgumentException(BadExpression);

                        Stack<string> subStack = GetStack(subFunc);
                        
                        //Adding subStack to calcStack
                        while (subStack.Count > 0)
                        {
                            revStack.Push(subStack.Pop());
                        }

                        while (revStack.Count > 0)
                        {
                            calcStack.Push(revStack.Pop());
                        }
                    }

                    calcStack.Push(buffStack.Pop());// Pop Name of func
                    continue;
                }

                // end
                if (func[i] == "end")
                {
                    if (buffStack.Peek() == "start") break;
                    if (buffStack.Peek() == "(") throw new Exception(BadAmountOfBrackets);
                    else calcStack.Push(buffStack.Pop());
                    continue;
                }

                calcStack.Push(func[i++]);
            }

            return calcStack;
        }

        public void CalcStack(Stack<string> stack)
        {
            if (stack == null)
            {
                throw new ArgumentNullException(nameof(stack));
            }

            if (stack.Count == 0)
            {
                throw new ArgumentException("stack is empty");
            }

            string element = stack.Pop();

            if (IsOperator(element))
            {
                if (stack.Count < 2) throw new Exception(BadExpression); //there must be 2 arguments  
                if (IsOperator(stack.Peek()) || funcs.ContainsKey(stack.Peek())) CalcStack(stack);
                if (stack.Count < 2)
                    throw
                        new Exception(
                            BadExpression); //there must be 1 argument 

                string operand_1 = stack.Pop();

                if (IsOperator(stack.Peek()) || funcs.ContainsKey(stack.Peek())) CalcStack(stack);
                if (stack.Count < 1)
                    throw
                        new Exception(
                            BadExpression); //there must be 1 argument

                string operand_2 = stack.Pop();

                double DoubleOperand_1 = 0;
                double DoubleOperand_2 = 0;

                DoubleOperand_1 = GetOperand(operand_1);
                DoubleOperand_2 = GetOperand(operand_2);

                switch (element)
                {
                    case "^":
                        DoubleOperand_1 = Math.Pow(DoubleOperand_2, DoubleOperand_1);
                        stack.Push(DoubleOperand_1.ToString());
                        break;
                    case "*":
                        DoubleOperand_1 = DoubleOperand_2 * DoubleOperand_1;
                        stack.Push(DoubleOperand_1.ToString());
                        break;
                    case "/":
                        DoubleOperand_1 = DoubleOperand_2 / DoubleOperand_1;
                        stack.Push(DoubleOperand_1.ToString());
                        break;
                    case "+":
                        DoubleOperand_1 = DoubleOperand_2 + DoubleOperand_1;
                        stack.Push(DoubleOperand_1.ToString());
                        break;
                    case "-":
                        DoubleOperand_1 = DoubleOperand_2 - DoubleOperand_1;
                        stack.Push(DoubleOperand_1.ToString());
                        break;
                }

                return;
            }

            Function function = null;
            if (funcs.ContainsKey(element)) function = funcs[element];
            if (userFunctions.ContainsKey(element)) function = userFunctions[element];

            if (function != null)
            {
                double[] args = new double[function.ArgsCount];
                for (int i = args.Length - 1; i >= 0; --i)
                {
                    if (stack.Count < i) throw new ArgumentException(BadExpression);
                    if (IsOperator(stack.Peek()) || funcs.ContainsKey(stack.Peek()) ||
                        userFunctions.ContainsKey(stack.Peek())) CalcStack(stack);
                    args[i] = GetOperand(stack.Pop());
                }

                stack.Push(function.func(args).ToString());
                return;
            }

            if (stack.Count >= 1) throw new ArgumentException(BadExpression);
        }

        private double GetOperand(string operand)
        {
            double DoubleOperand;
            if (Args.ContainsKey(operand)) DoubleOperand = Args[operand];
            else
            {
                try
                {
                    DoubleOperand = double.Parse(operand);
                }
                catch (Exception e)
                {
                    throw new ArgumentException(BadName);
                }
            }

            return DoubleOperand;
        }

        public double CalcFunction(string expression)
        {
            List<string> func = GetList(expression);
            Stack<string> result = GetStack(func);
            CalcStack(result);
            if (result.Count > 1) throw new Exception(BadExpression);
            return double.Parse(result.Pop());
        }
    }
}