﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SimpleCalculator
{
    class Function
    {
        public delegate double Func(params double[] args);

        public int ArgsCount { get; set; }
        public Func func;
    
        public Function(Func func, int argsCount)
        {
            this.func = func;
            ArgsCount = argsCount;
        }
    }
}
